
if [ -z "${SSL_DOMAIN}" ]; then
    echo "You must specify the SSL_DOMAIN ENV VAR for this script to work"
    exit 1
fi

if [ -z "${SSL_EMAIL}" ]; then
   echo "You must specify the SSL_EMAIL ENV VAR for this script to work"
   exit 1
fi

if [ -z "${SSL_CERTBOTPORT}" ]; then
    SSL_CERTBOTPORT=8888
fi


certbot certonly -d ${SSL_DOMAIN} --standalone -n --agree-tos --email ${SSL_EMAIL} --http-01-port=${SSL_CERTBOTPORT}

#Combine fullchain and the private key into a single file
cat /etc/letsencrypt/live/${SSL_DOMAIN}/fullchain.pem /etc/letsencrypt/live/${SSL_DOMAIN}/privkey.pem > /etc/letsencrypt/live/${SSL_DOMAIN}.pem
