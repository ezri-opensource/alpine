# Docker : EzriNumbus/alpine

## Description
This is a custom build alpine distribution that adds in several basic toolset.  Each tool is described below.


## Env Variables

Variable | Default | Description
:--------|:--------|:------------
SSL_CERTBOTPORT | 8888 | What port Let's encrypt CERTBOT should listen on
SSL_DOMAIN | -- no default -- | What domain should we generate the certificate for
SSL_EMAIL | -- no default -- | The email address for SSL certs through certbot


## Let's Encrypt : SSL Cert Generator

1. Create a Dockerfile similar to this one, updating `SSL_DOMAIN` and `SSL_EMAIL` to reflect your HTTPS domain, and an email of your choice
 
* The SSL_CERTBOTPORT is the port to run the certbot server on. Defaults to 8888

 ```
 FROM ezrinumbus/aline
 ENV SSL_DOMAIN mydomain.com
 ENV SSL_EMAIL certs@mydomain.com
 ENV SSL_CERTBOTPORT 8888
 ```

1. Build the container `docker build -t myContainerName`
1. Generate the certificate by using `docker run exec myContainerName /tools/lets-encrypt/get-certificate.sh
1. Launch the container `docker run myContainerName`

* Cert will be saved at /etc/letsencrypt/live/${SSL_DOMAIN}/fullchain.pem
* Key will be saved at /etc/letsencrypt/live/${SSL_DOMAIN}/privkey.pem
* Cert wila expire 3 months after generation. Run `certbot renew` to renwa all certs
